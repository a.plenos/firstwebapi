﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.DAL
{
    public class TodoRepository : ITodoRepository
    {
        private readonly TodoContext _context;

        public TodoRepository(TodoContext context)
        {
            _context = context;
        }

        public virtual async Task<TodoItemDTO> Get(long id)
        {
            return await _context.TodoItemsDTO.FindAsync(id);
        }

        public virtual async Task<IEnumerable<TodoItemDTO>> GetAll()
        {
            return await _context.TodoItems
                .Select(x => ItemToDTO(x))
                .ToListAsync();
        }

        public async Task<bool> AddItem(TodoItemDTO todoItemDTO)
        {
            var todoItem = new TodoItem
            {
                IsComplete = todoItemDTO.IsComplete,
                Name = todoItemDTO.Name
            };

            _context.TodoItems.Add(todoItem);
            await _context.SaveChangesAsync();
            return true;
        }

        public virtual async Task<bool> UpdateItem(TodoItemDTO todoItemDTO)
        {
            _context.TodoItemsDTO.Update(todoItemDTO);
            await _context.SaveChangesAsync();
            return true;
        }

        public virtual async Task<bool> DeleteItem(TodoItemDTO todoItemDTO)
        {
            _context.TodoItemsDTO.Remove(todoItemDTO);
            await _context.SaveChangesAsync();
            return true;
        }

        private static TodoItemDTO ItemToDTO(TodoItem todoItem) =>
            new TodoItemDTO
            {
                Id = todoItem.Id,
                Name = todoItem.Name,
                IsComplete = todoItem.IsComplete
            };
    }
}
