﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.DAL
{
    public interface ITodoRepository
    {
        Task<IEnumerable<TodoItemDTO>> GetAll();
        Task<TodoItemDTO> Get(long id);
        Task<bool> AddItem(TodoItemDTO todoItemDTO);
        Task<bool> UpdateItem(TodoItemDTO todoItemDTO);
        Task<bool> DeleteItem(TodoItemDTO todoItemDTO);
    }
}
