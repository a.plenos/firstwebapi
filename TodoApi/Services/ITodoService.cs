﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.Services
{
    public interface ITodoService
    {
        Task<IEnumerable<TodoItemDTO>> GetAll();
        Task<TodoItemDTO> Get(long id);
        Task<bool> AddItem(TodoItemDTO todoItemDTO);
        Task<bool> UpdateItem(long id, TodoItemDTO todoItemDTO);
        Task<bool> DeleteItem(long id);
    }
}
