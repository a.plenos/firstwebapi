﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TodoApi.Models;
using TodoApi.DAL;

namespace TodoApi.Services
{
    public class TodoService : ITodoService
    {
        private readonly ITodoRepository _todoRepository;

        public TodoService(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public virtual async Task<TodoItemDTO> Get(long id)
        {
            return await _todoRepository.Get(id);
        }

        public virtual async Task<IEnumerable<TodoItemDTO>> GetAll()
        {
            return await _todoRepository.GetAll();
        }

        public async Task<bool> AddItem(TodoItemDTO todoItemDTO)
        {
            return await _todoRepository.AddItem(todoItemDTO);
        }

        public virtual async Task<bool> UpdateItem(long id, TodoItemDTO todoItemDTO)
        {
            var todo = await _todoRepository.Get(id);
            if (todo == null)
            {
                return false;
            }

            return await _todoRepository.UpdateItem(todoItemDTO);
        }

        public virtual async Task<bool> DeleteItem(long id)
        {
            var todo = await _todoRepository.Get(id);
            if (todo == null)
            {
                return false;
            }

            return await _todoRepository.DeleteItem(todo);
        }
    }
}
